require 'rubygems'
require 'mp3info'

#class representing one music item
class MusicItem

  attr_accessor :filename, :filename_short
  attr_accessor :tags, :tags1, :tag_count
  #for mp3info features look at: http://ruby-mp3info.rubyforge.org/
  attr_accessor :info, :s_length
  attr_accessor :player

  def initialize filename
    @filename = filename
    @filename_short = File.basename(@filename, ".mp3")
    update_mp3info
    @s_length = calculate_length @info.length
  end

  def init_player
    #ok, this might be overkill, but whatever
    #every musicitem has it's own Player
    if @player.nil?
      @player = Player.new @filename, @info
    end
  end

  #tag file and update mp3info
  def tag newtag
    #we obviously only want to do that only if it isn't already tagged with this tag right?
    Thread.new do
      unless @tags.include? newtag
        seperator = " "
        seperator = "" if @tags.eql? ""
        @tags = "#{@tags}#{seperator}#{newtag}"
        @info.tag2.TXXX = @tags
        @info.close
        update_mp3info
      end
    end
  end

  def update_mp3info
    Mp3Info.open(@filename) do |mp3info|
      @info = mp3info
      #id3v1 tags
      @tags1 = mp3info.tag1
      #the id3v2 user text tag we use to handle our tags
      @tags = mp3info.tag2.TXXX
      @tags = "" if @tags.nil?
      @tag_count = @tags1.length
    end
  end


  #generate length of format mm:ss
  def calculate_length length
    minutes = (length/60).to_s.split(".").first
    seconds = (length%60).to_s.split(".").first
    seconds = "0#{seconds}" if seconds.to_i < 9
    return "#{minutes}:#{seconds}"
  end
end