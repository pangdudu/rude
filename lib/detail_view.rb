require 'gtk2'
require 'waveform_narray.rb'

class DetailView

  attr_accessor :view
  attr_accessor :mi
  attr_accessor :autocompletion
  attr_accessor :tag_cache

  def initialize musicitem,tag_cache
    @tag_cache = tag_cache
    @visible_tags = []
    @taglines = []
    @dummy_tags = ["delete","electro","house","minimal"]
    @mi = musicitem
    # rows, columns, homogeneous
    @view = Gtk::VBox.new(false,0)
    #test label
    @label =Gtk::Label.new(
      "detailed view:\n" +
        "title: #{@mi.tags1["title"]}\n" +
        "filename: #{@mi.filename_short}\n" +
        "bitrate: #{@mi.info.bitrate} kbps - samplerate: #{@mi.info.samplerate} kHz - length: #{@mi.s_length}\n" +
        "tags: #{@mi.tags}"
    )
    labelbox = Gtk::VBox.new(true,0)
    labelbox.add @label
    tagflushbutton = Gtk::Button.new("Flush tags!")
    tagflushbutton.signal_connect("clicked") {flushtags}
    tagflushbutton.set_height_request(0)
    tagflushbutton.set_width_request(0)
    labelbox.add tagflushbutton
    waveform = get_waveform
    @tagcontrol = get_tagcontrol
    tagbox = Gtk::HBox.new(false,0)
    tagbox.add labelbox
    tagbox.add @tagcontrol
    @view.add tagbox
    @view.add waveform
    #for fraction calculation
    @offset = 5
    @waveformwidth = 1024
  end

  #build the tagcontrol widget
  def get_tagcontrol
    #here is where the auto completion entries will be stored
    @autocompletion = Gtk::ListStore.new(String)
    #setup model with some default values
    fill_completion_with_default_values
    #add tags from tag cache to completion
    @tag_cache.each { |key,value| fill_completion_with_values key }
    #this handles the completion for the input widget
    completion = Gtk::EntryCompletion.new
    completion.set_model @autocompletion
    #need to set, where to look for completion in model
    completion.set_text_column(0)
    #the input widget
    input = Gtk::Entry.new
    input.set_completion completion
    input.signal_connect("activate") {add_tag_button input,input.text}
    #a box we put our input entry into and add a nice description
    inputwithlabel = Gtk::VBox.new(false,0)
    inputlabel =Gtk::Label.new("What do you want your file to be tagged with:")
    inputwithlabel.add(inputlabel)
    inputwithlabel.add(input)
    @tag_buttons = Gtk::VBox.new(false,0)
    #the table everything will be put into
    tagcontrol = Gtk::VBox.new(false,0)
    tagcontrol.add(inputwithlabel)
    tagcontrol.add(@tag_buttons)
    #add the dummy tag buttons
    @dummy_tags.each { |tagname| add_tag_button input,tagname }
    #add the tags already used on the file
    @mi.tags.split(" ").each { |tagname| add_tag_button input,tagname} unless @mi.nil?
    return tagcontrol
  end

  #add a tag button
  def add_tag_button widget,tagname
    #only add the button if it isn't already displayed
    unless @visible_tags.include? tagname
      @visible_tags << tagname
      #puts "Adding tag button: #{tagname}"
      tag_button = Gtk::Button.new(tagname)
      tag_button.signal_connect("clicked") {tag_button_pressed tag_button}
      #check if theres already a tagline and add button
      unless @taglines.empty?
        if @taglines.last.children.length >= 5
          # button box for our tags
          tagline = Gtk::HButtonBox.new
          tagline.set_height_request(0)
          tagline.set_width_request(0)
          tagline.set_layout_style "CENTER"
          tagline.add tag_button
          @taglines << tagline
          @tag_buttons.add tagline
        end
        if @taglines.last.children.length < 5
          tagline = @taglines.last
          tagline.add tag_button
        end
      end
      #no taglines yet?
      if @taglines.empty?
        # button box for our tags
        tagline = Gtk::HButtonBox.new
        tagline.set_height_request(0)
        tagline.set_width_request(0)
        tagline.set_layout_style "CENTER"
        tagline.add tag_button
        @taglines << tagline
        @tag_buttons.add tagline
      end
    end
    @tag_buttons.show_all
    #reset text input
    widget.set_text ""
  end

  #executed when a tag button is pressed
  def tag_button_pressed button
    Thread.new do
      #puts "Tagged musicitem with tag: #{button.label}"
      @mi.tag button.label
      @label.set_text(
        "title: #{@mi.tags1["title"]}\n" +
          "filename: #{@mi.filename_short}\n" +
          "bitrate: #{@mi.info.bitrate} kbps - samplerate: #{@mi.info.samplerate} kHz - length: #{@mi.s_length}\n" +
          "tags: #{@mi.tags}"
      )
      #puts "Tags now: #{@mi.tags}"
    end
  end

  #flush the tags
  def flushtags
    Thread.new do
      @mi.info.tag2.TXXX = ""
      @mi.info.close
      @mi.update_mp3info
      @label.set_text(
        "title: #{@mi.tags1["title"]}\n" +
          "filename: #{@mi.filename_short}\n" +
          "bitrate: #{@mi.info.bitrate} kbps - samplerate: #{@mi.info.samplerate} kHz - length: #{@mi.s_length}\n" +
          "tags: #{@mi.tags}"
      )
    end
  end

  #fill the autocompletion model with some default values
  def fill_completion_with_default_values
    @dummy_tags.each do |entry|
      iter = @autocompletion.append
      @autocompletion.set_value(iter, 0, entry)
    end
  end

  #fill the autocompletion model with values from an array
  def fill_completion_with_values tags
    tags.each do |entry|
      iter = @autocompletion.append
      @autocompletion.set_value(iter, 0, entry)
    end
  end

  #get waveform image and player control widget
  def get_waveform
    #ok, waveform creation takes some time,
    #so we don't want to do it, if we don't have to.
    image_file = "images/#{@mi.filename_short}.png"
    unless File.exist? image_file
      waveform = Waveform.new 1024, @mi.filename , image_file
      waveform.create_waveform_image
      @offset = waveform.offset
      @waveformwidth = waveform.width
    end
    image = Gtk::Image.new(image_file)
    event_box = Gtk::EventBox.new.add(image)
    event_box.signal_connect("button_press_event") do |widget,event|
      fraction = calculate_mi_fraction image,event
      puts "Fraction of the waveform clicked: #{fraction}"
      @mi.player.seek fraction
    end
    return event_box
  end

  #calculates the fraction of the audio track from the clicked x position of the waveform image
  def calculate_mi_fraction widget,event
    fraction = 0.0
    border = (widget.window.geometry[2] - @waveformwidth)/2
    x = event.x - border - @offset/2
    fraction = (x/@waveformwidth).to_f
    return fraction
  end
end



