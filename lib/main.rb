#RUDE is supposed to become a more less fancy
#music organization and dj-set creation tool.
#
#Check out the ongoing progress at: http://github.com/pangdudu/rude/
#
#Have fun!

require 'gtk2'
#require 'xspf'
require 'player.rb'
require 'musicitem.rb'
require 'detail_view.rb'
require 'playlist.rb'

$app_title_and_version = "RUDE - Ruby Uber Deejaying organizEr - v0.2 (Taco Loco)"

#the "main" class with all the spicy gui salsa inside
class PlayerGui

  attr_accessor :mainwindow
  attr_accessor :gui_items
  attr_accessor :playlist, :playlist_model, :playlist_cache
  attr_accessor :nowplaying
  attr_accessor :autowaveform, :waveforming
  attr_accessor :tag_cache
  attr_accessor :status_info

  def initialize
    #autowaveform generation toggle
    @autowaveform = true
    @waveforming = false
    #displays status info
    @status_info = Gtk::Label.new "initialized..."
    #Hash storing the MusicItems
    @playlist_cache = Hash.new("no data")
    #current session tag cache
    @tag_cache = Hash.new("no tags")
    #Hash holding all the initial gui items
    @gui_items = init_gui_items
    #init the main (sort of toplevel) window
    @mainwindow = init_main_window
    #setup the window layout
    setup_window_layout
    #display the whole enchilada
    @mainwindow.show_all
  end

  #setup the main window
  def init_main_window
    window = Gtk::Window.new(Gtk::Window::TOPLEVEL)
    window.set_title  $app_title_and_version
    window.border_width = 10
    window.set_size_request(1030, 500)
    window.signal_connect('delete_event') { rude_die }
    #add a signal to trigger destruction
    window.signal_connect("destroy") { rude_die }
    return window
  end

  #setup all initial gui elements
  def init_gui_items
    #this hash will later on become @items
    hash = Hash.new("no button")
    #hash with all buttons for the menubar
    hash["menu_items"] = Hash.new("no button")
    #button opening a filechooser
    hash["menu_items"]["open"] = Gtk::Button.new("add files to playlist")
    hash["menu_items"]["open"].set_width_request(-1)
    hash["menu_items"]["open"].signal_connect("clicked") {update_playlist}
    #add the status info
    hash["menu_items"]["status"] = @status_info
    #hash with the pane items
    hash["vpane_items"] = Hash.new("no button")
    #playlist is at the bottom of the pane
    hash["vpane_items"]["playlist"] = init_playlist
    return hash
  end

  #init the playlist
  def init_playlist
    treeview = Gtk::TreeView.new
    #tweak our little treeview
    configure_treeview treeview
    setup_treeview treeview
    # Create a new tree model
    @playlist_model = Gtk::ListStore.new(String,String,String,String)
    #update the playlist data model
    update_playlist
    # Add the tree model to the tree view
    treeview.model = @playlist_model
    # Now we put everything in a scrolled window
    scrolled_win = Gtk::ScrolledWindow.new
    scrolled_win.add(treeview)
    scrolled_win.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC)
    return scrolled_win
  end

  #some special options for the treeview
  def configure_treeview treeview
    treeview.set_headers_visible true
    treeview.columns_autosize
    treeview.show_expanders = true
    treeview.set_headers_clickable true
    #funky selection magic
    selection = treeview.selection
    selection.mode = Gtk::SELECTION_BROWSE
    treeview.signal_connect( "row-activated" ) { display_selection selection }
  end

  #displays the selected row in detail view
  def display_selection selection
    selection.selected_each do |mod, path, iter|
      filename_short = @playlist_model.get_value(iter,2)
      musicitem = @playlist_cache[filename_short]
      update_detailed_view musicitem
    end
  end

  #here we update the detailed view
  #detail view consists of all the nice stuff: waveform, tagging, infos
  def update_detailed_view musicitem
    if musicitem.instance_of?(MusicItem)
      #stop the last playing item
      if @nowplaying.instance_of?(MusicItem)
        @nowplaying.player.stop
      end
      #and set the newly added musicitem to be our current playing one
      @nowplaying = musicitem
      @nowplaying.init_player
      #remove old detailed view
      @gui_items["vpane"].remove(@gui_items["vpane_items"]["detailed_view"])
      #create new detailed view
      detail_view = DetailView.new musicitem,@tag_cache
      @gui_items["vpane_items"]["detailed_view"] = detail_view.view
      @gui_items["vpane_items"]["detailed_view"].set_height_request(0)
      #pack and redraw
      @gui_items["vpane"].pack1(@gui_items["vpane_items"]["detailed_view"],true,false)
      @gui_items["vpane_items"]["detailed_view"].set_height_request(0)
      @gui_items["vpane"].show_all
      #start playing
      pushinfo "playing"
      @nowplaying.player.play
    end
  end

  #update the playlist
  def update_playlist
    filenames = open_file_chooser
    return if filenames.nil?
    #setup empty playlist, array holds MusicItems
    @playlist = Array.new
    #build music items from filenames
    filenames.each do |filename|
      musicitem = MusicItem.new(filename)
      #this is to update the treeview
      @playlist << musicitem
      #and this, so we can later access the musicitems
      @playlist_cache[musicitem.filename_short] = musicitem
    end
    update_playlist_model
    updatewaveforms
  end

  #update the playlist model
  def update_playlist_model
    update_playlist if @playlist.length < 1
    # Add all of the music items to the GtkListStore
    @playlist.each_with_index do |e, i|
      iter = @playlist_model.append
      @playlist_model.set_value(iter, 0, @playlist[i].tags1["title"])
      @playlist_model.set_value(iter, 1, @playlist[i].tags1["artist"])
      @playlist_model.set_value(iter, 2, @playlist[i].filename_short)
      @playlist_model.set_value(iter, 3, @playlist[i].s_length)
    end
  end

  #method managing background waveform creation
  def updatewaveforms
    if @autowaveform && !@waveforming
      Thread.new do
        pushinfo "starting background auto waveform generation"
        @waveforming = true
        @playlist_cache.each do |filename,musicitem|
          pushinfo "generating waveform for #{filename}"
          #ok, waveform creation takes some time,
          #so we don't want to do it, if we don't have to.
          image_file = "images/#{filename}.png"
          unless File.exist? image_file
            waveform = Waveform.new 1024, musicitem.filename , image_file
            waveform.create_waveform_image
          end
          pushinfo "waveform for #{filename} generated"
        end
        @waveforming = false
      end
    end
  end

  # Add columns to the GtkTreeView.
  def setup_treeview treeview
    add_column treeview, "title", 0
    add_column treeview, "artist", 1
    add_column treeview, "filename", 2
    add_column treeview, "length (seconds)", 3
  end

  #push new info onto status label
  def pushinfo text
    text = text[0..60]
    text += " ..."
    @status_info.set_text text
  end

  # Create a new GtkCellRendererText, add it to the tree
  # view column and append the column to the tree view.
  def add_column treeview, title, number
    renderer = Gtk::CellRendererText.new
    column = Gtk::TreeViewColumn.new(title, renderer, "text" => number)
    treeview.append_column(column)
  end


  #setup general layout
  def setup_window_layout
    #notebook holding everything
    nb = Gtk::Notebook.new
    # table holding menu and hpane
    # rows, columns, homogeneous
    table = Gtk::Table.new(2, 1, false)
    # button box for our menu
    @gui_items["menu"] = Gtk::HButtonBox.new
    @gui_items["menu"].set_layout_style "START"
    @gui_items["menu"].set_height_request(0)
    @gui_items["menu_items"].each do |name,button|
      # child, expand, fill, padding
      @gui_items["menu"].pack_start(button, false, false, 5)
    end
    # resizable pane with playlist and detail view
    @gui_items["vpane"] = Gtk::VPaned.new
    @gui_items["vpane_items"]["detailed_view"] = Gtk::Label.new(
      "detailed view:\n" +
        "riva, riva, andale!"
    )
    @gui_items["vpane_items"]["detailed_view"].set_height_request(0)
    @gui_items["vpane"].pack1(@gui_items["vpane_items"]["detailed_view"],true,false)
    @gui_items["vpane"].pack2(@gui_items["vpane_items"]["playlist"],true,false)
    #table options
    shrink_options = Gtk::SHRINK
    expand_options = Gtk::EXPAND|Gtk::FILL
    # child, x1, x2, y1, y2, x-opt,   y-opt,   xpad, ypad
    table.attach(@gui_items["vpane"],  0,  1,  0,  1, expand_options, expand_options, 0, 0)
    table.attach(@gui_items["menu"],  0,  1,  1,  2, shrink_options, shrink_options, 0, 0)
    nb.append_page table, Gtk::Label.new("player")
    nb.append_page Gtk::Label.new("organizer"),Gtk::Label.new("organizer")
    @mainwindow.add(nb)
  end

  #opens a filechooser and returns an array of filenames
  def open_file_chooser
    dialog = Gtk::FileChooserDialog.new("choose your audative destiny!", @mainwindow, Gtk::FileChooser::ACTION_OPEN,
      nil,[Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],[Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    #set so we can select multiple files at once
    dialog.select_multiple = true
    #filters that help the user choose the correct files
    filter = Gtk::FileFilter.new
    filter.name = "music files"
    filter.add_pattern('*.mp3')
    #ogg support
    #filter.add_pattern('*.ogg')
    dialog.add_filter(filter)
    #if we get a user response, return the filenames
    if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
      filenames = dialog.filenames
    end
    dialog.destroy
    return filenames
  end

  #start actual gstreamer audio player
  def play filename
    stop_player 
    @player = Player.new filename
    @player.loop
    stop_player
  end

  #kill gui and stop player
  def rude_die
    stop_player
    Gtk.main_quit
  end

  #stop gst player
  def stop_player
    @player.stop  if @player.instance_of?(Player)
  end

end

#let'se go!
gui = PlayerGui.new
Gtk.main

#That's all folks!